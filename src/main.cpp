#include <Arduino.h>

#ifndef degreeIconDef
#include <characters/degree.hpp>
#endif

// Include the libraries:
// LiquidCrystal_I2C.h: https://github.com/johnrickman/LiquidCrystal_I2C
// Include the libraries:
// LiquidCrystal_I2C.h: https://github.com/johnrickman/LiquidCrystal_I2C

#include <Wire.h> // Library for I2C communication
#include <LiquidCrystal_I2C.h> // Library for LCD

// Wiring: SDA pin is connected to A4 and SCL pin to A5.
// Connect to LCD via I2C, default address 0x27 (A0-A2 not jumpered)
LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 20, 4); // Change to (0x27,16,2) for 16x2 LCD.my address is (0x3F)

void setup() {
  Serial.begin(9600);
  lcd.init();
  lcd.backlight();

}

void loop() {

  // Serial.println("hey");k
  lcd.begin(20, 4);
  lcd.createChar(0, degreeIcon);


  // Print 'Hello World!' on the first line of the LCD:
  lcd.setCursor(0, 0); // Set the cursor on the first column and first row.(7->column, 0->row)
  lcd.print("Ceres N Meses        "); // Print the string "Hello World!"
  
  lcd.setCursor(0, 1); //Set the cursor on the third column and the second row (counting starts at 0!).row 1
  for (int i = 1; i <= 20; i++)
  {
    lcd.print("-");
  }

  lcd.setCursor(4, 2); //Set the cursor on the third column and the second row (counting starts at 0!).row 2
  lcd.print("MKINVENTIONS");

  lcd.setCursor(2, 3); //Set the cursor on the third column and the second row (counting starts at 0!).row 3
  lcd.print("MADHAN CHIRUGURI");

  delay(5000);



    // Print 'Hello World!' on the first line of the LCD:
  lcd.setCursor(0, 0); // Set the cursor on the first column and first row.(7->column, 0->row)
  lcd.print("ceres-n-meses.now.sh"); // Print the string "Hello World!"
  
  lcd.setCursor(0, 1); //Set the cursor on the third column and the second row (counting starts at 0!).row 1
  for (int i = 1; i <= 20; i++)
  {
    lcd.print("-");
  }

  lcd.setCursor(4, 2); //Set the cursor on the third column and the second row (counting starts at 0!).row 2
  lcd.print("Humidity: 39%");

  lcd.setCursor(2, 3); //Set the cursor on the third column and the second row (counting starts at 0!).row 3
  lcd.print("Temperature: 20");
  lcd.write(0);
  lcd.print("C");

  // lcd.rightToLeft();
  delay(5000);
}