#include <Arduino.h>
// Include the libraries:
// LiquidCrystal_I2C.h: https://github.com/johnrickman/LiquidCrystal_I2C
// Include the libraries:
// LiquidCrystal_I2C.h: https://github.com/johnrickman/LiquidCrystal_I2C

#include <Wire.h> // Library for I2C communication
#include <LiquidCrystal_I2C.h> // Library for LCD

// Wiring: SDA pin is connected to A4 and SCL pin to A5.
// Connect to LCD via I2C, default address 0x27 (A0-A2 not jumpered)
LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 20, 4); // Change to (0x27,16,2) for 16x2 LCD.my address is (0x3F)

void setup() {
  Serial.begin(9600);
  lcd.init();
  lcd.backlight();
}

void loop() {
  // Print 'Hello World!' on the first line of the LCD:
  lcd.setCursor(7, 0); // Set the cursor on the first column and first row.(7->column, 0->row)
  lcd.print("HELLO!"); // Print the string "Hello World!"
  
  lcd.setCursor(5, 1); //Set the cursor on the third column and the second row (counting starts at 0!).row 1
  lcd.print("WELCOME TO");

  lcd.setCursor(4, 2); //Set the cursor on the third column and the second row (counting starts at 0!).row 2
  lcd.print("MKINVENTIONS");

  lcd.setCursor(2, 3); //Set the cursor on the third column and the second row (counting starts at 0!).row 3
  lcd.print("MADHAN CHIRUGURI");
  Serial.println("hey");
}